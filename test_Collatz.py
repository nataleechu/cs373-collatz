#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ---------------
# test_Collatz.py
# ---------------

# -------
# imports
# -------

import unittest  # main, TestCase

from Collatz import collatz_eval
from Collatz import cycle_finder

# ------------
# Test_Collatz
# ------------


class Test_Collatz(unittest.TestCase):

    # my unit tests

    def test_collatz_0(self) -> None:
        self.assertEqual(collatz_eval((567890, 123456)), (567890, 123456, 470))

    def test_collatz_1(self) -> None:
        self.assertEqual(collatz_eval((555, 777)), (555, 777, 171))

    def test_collatz_2(self) -> None:
        self.assertEqual(collatz_eval((99999, 11111)), (99999, 11111, 351))

    def test_collatz_3(self) -> None:
        self.assertEqual(collatz_eval((2, 100000)), (2, 100000, 351))

    def test_collatz_4(self) -> None:
        self.assertEqual(collatz_eval((5, 5)), (5, 5, 6))

    def test_collatz_5(self) -> None:
        self.assertEqual(collatz_eval((300, 10000)), (300, 10000, 262))

    def test_collatz_6(self) -> None:
        self.assertEqual(collatz_eval((20, 10)), (20, 10, 21))

    def test_collatz_7(self) -> None:
        self.assertEqual(collatz_eval((90000, 1000)), (90000, 1000, 351))

    def test_collatz_8(self) -> None:
        self.assertEqual(collatz_eval((1, 2)), (1, 2, 2))

    def test_collatz_9(self) -> None:
        self.assertEqual(collatz_eval((99, 98)), (99, 98, 26))

    def test_collatz_10(self) -> None:
        self.assertEqual(collatz_eval((20, 200)), (20, 200, 125))

    def test_collatz_11(self) -> None:
        self.assertEqual(collatz_eval((99000, 99001)), (99000, 99001, 116))

    def test_collatz_12(self) -> None:
        self.assertEqual(collatz_eval((5, 1)), (5, 1, 8))

    def test_collatz_13(self) -> None:
        self.assertEqual(collatz_eval((2048, 4096)), (2048, 4096, 238))

    def test_collatz_14(self) -> None:
        self.assertEqual(collatz_eval((10, 17)), (10, 17, 18))

    def test_collatz_15(self) -> None:
        self.assertEqual(collatz_eval((9000, 35589)), (9000, 35589, 311))

    def test_collatz_16(self) -> None:
        self.assertEqual(collatz_eval((314, 349)), (314, 349, 144))

    def test_collatz_17(self) -> None:
        self.assertEqual(cycle_finder(11222), 162)

    def test_collatz_18(self) -> None:
        self.assertEqual(cycle_finder(500), 111)

    def test_collatz_19(self) -> None:
        self.assertEqual(cycle_finder(5), 6)



# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
