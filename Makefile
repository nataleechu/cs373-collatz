.DEFAULT_GOAL := all
SHELL         :=  bash

ifeq ($(shell uname -s), Darwin)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint
    PYTHON        := python3
else ifeq ($(shell uname -p), x86_64)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint
    PYTHON        := python3.11
else
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage
    MYPY          := mypy
    PYDOC         := pydoc
    PYLINT        := pylint
    PYTHON        := python
endif

# run docker
docker:
	docker run --rm -i -t -v $(PWD):/usr/python -w /usr/python gpdowning/python

# get git config
config:
	git config -l

# get git log
Collatz.log.txt:
	git log > Collatz.log.txt

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Collatz code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Collatz code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	-git add Collatz.html
	-git add Collatz.log.txt
	git add Collatz.py
	git add Makefile
	git add README.md
	git add run_Collatz.ctd.txt
	git add run_Collatz.py
	git add test_Collatz.py
	git commit -m "another commit"
	git push
	git status

all:

# execute test harness with coverage
test:
	$(COVERAGE) run --branch test_Collatz.py
	$(COVERAGE) report -m

# clone the Collatz test repo
../cs373-collatz-tests:
	git clone https://gitlab.com/gpdowning/cs373-collatz-tests.git ../cs373-collatz-tests

# test files in the Collatz test repo
T_FILES := `ls ../cs373-collatz-tests/*.in.txt`

# generate a random input file
ctd-generate:
	for v in {1..100}; do $(CHECKTESTDATA) -g run_Collatz.ctd.txt >> run_Collatz.gen.txt; done

# execute the run harness against a test file in the Collatz test repo and diff with the expected output
../cs373-collatz-tests/%:
	$(CHECKTESTDATA) run_Collatz.ctd.txt $@.in.txt
	./run_Collatz.py < $@.in.txt > run_Collatz.tmp.txt
	diff run_Collatz.tmp.txt $@.out.txt

# execute the run harness against your test files in the Collatz test repo and diff with the expected output
run: ../cs373-collatz-tests
	$(MYPY)     Collatz.py
	$(MYPY)     test_Collatz.py
	$(PYLINT)   Collatz.py
	$(PYLINT)   test_Collatz.py
	make ../cs373-collatz-tests/nataleechu-run_Collatz # change gpdowning to your GitLab-ID

# execute the run harness against all of the test files in the Collatz test repo and diff with the expected output
run-all: ../cs373-collatz-tests
	$(MYPY)     Collatz.py
	$(MYPY)     test_Collatz.py
	$(PYLINT)   Collatz.py
	$(PYLINT)   test_Collatz.py
	-for v in $(T_FILES); do make $${v/.in.txt/}; done

# auto format the code
format:
	$(BLACK) Collatz.py
	$(BLACK) run_Collatz.py
	$(BLACK) test_Collatz.py

# create html file
Collatz.html: Collatz.py
	-$(PYDOC) -w Collatz

# check files, check their existence with make check
C_FILES :=          \
    .gitignore      \
    .gitlab-ci.yml  \
    Collatz.html    \
    Collatz.log.txt

# check the existence of check files
check: $(C_FILES)

# remove temporary files
clean:
	rm -f  .coverage
	rm -f  *.gen.txt
	rm -f  *.tmp.txt
	rm -rf __pycache__
	rm -rf .mypy_cache

# remove temporary files and generated files
scrub:
	make clean
	rm -f Collatz.html
	rm -f Collatz.log.txt

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which $(BLACK)
	@echo
	$(BLACK) --version | head -n 1

	@echo
	which $(CHECKTESTDATA)
	@echo
	$(CHECKTESTDATA) --version | head -n 1

	@echo
	which $(COVERAGE)
	@echo
	$(COVERAGE) --version | head -n 1

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

	@echo
	which $(MYPY)
	@echo
	$(MYPY) --version

	@echo
	which pip
	@echo
	pip --version

	@echo
	which $(PYDOC)

	@echo
	which $(PYLINT)
	@echo
	$(PYLINT) --version | head -n 1

	@echo
	which $(PYTHON)
	@echo
	$(PYTHON) --version

	@echo
	which vim
	@echo
	vim --version | head -n 1
