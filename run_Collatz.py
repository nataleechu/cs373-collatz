#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = pointless-string-statement

# --------------
# run_Collatz.py
# --------------

# -------
# imports
# -------

from Collatz import collatz_eval

# ------------
# collatz_read
# ------------


def collatz_read() -> tuple[int, int]:
    i, j = [int(s) for s in input().split()]
    return i, j


# -------------
# collatz_print
# -------------


def collatz_print(t: tuple[int, int, int]):
    i, j, v = t
    print(i, j, v)


# ----
# main
# ----


def main() -> None:
    try:
        while True:
            collatz_print(collatz_eval(collatz_read()))
    except EOFError:
        pass


if __name__ == "__main__":  # pragma: no cover
    main()

"""
sample input

1 10
100 200
201 210
900 1000
"""

"""
sample output

1 10 20
100 200 125
201 210 89
900 1000 174
"""
