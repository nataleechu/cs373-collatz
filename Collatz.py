#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ----------
# Collatz.py
# ----------

# ------------
# collatz_eval
# ------------
# import sys

cache = {1: 1}


def collatz_eval(t: tuple[int, int]) -> tuple[int, int, int]:
    i, j = t
    assert i > 0
    assert j > 0

    start = i
    end = j
    # range optimization
    if start > end:
        end = start
        start = j
    if end // 2 > start:
        start = end // 2 + 1

    max_count = 0
    for x in range(start, end + 1):
        count = cycle_finder(x)
        if count > max_count:
            max_count = count

    mytuple = (i, j, max_count)
    return mytuple


# finds the number of cycles for given int x
def cycle_finder(x):
    if x in cache:
        return cache[x]

    count = 1
    cur_value = x

    while cur_value > 1:
        if (cur_value % 2) == 0:
            cur_value = cur_value // 2
        else:
            cur_value = cur_value + (cur_value >> 1) + 1
            count += 1
        count += 1
        # uses information from cache if available
        if cur_value in cache:
            count = count + cache[cur_value] - 1
            break

    assert count > 0
    # updates cache with new value
    cache[x] = count

    return count
